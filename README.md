# IFC Project Creation 

A repository that enables the creation of new IFC files with multiple walls. A small alteration to the [code](https://academy.ifcopenshell.org/posts/creating-a-simple-wall-with-property-set-and-quantity-information/)
 published by "Kianwee Chen" on [IfcOpenShell Academy](https://academy.ifcopenshell.org/).

## Description

The repo is composed of a Python code that is created as a part of the Construction and Robotics master program at RWTH Aachen University. It aims to create a brand new IFC model with all the required data structure and can be further developed.


## Requirements

Make sure to download the required IfcOpenShells Python library from this [link](http://ifcopenshell.org/python).
